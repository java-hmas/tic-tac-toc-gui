/*
* IB Computer Science Higher Level - 2018
* Humberto Alvarez 
* 2-player Tic-Tac-Toe Java implementation with Graphic User Interface
*/

package pckge;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JButton;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import net.miginfocom.swing.MigLayout;
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import javax.swing.SpringLayout;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class TicTacToe 
{
	private JFrame frame;
	public static int turns;
	public static String WINNER = "no one";
	public static String player;
	public static boolean GG = false;
	public static String A;
	public static String B;
	public static String C;
	public static String D;
	public static String E;
	public static String F;
	public static String G;
	public static String H;
	public static String I;
	private JTextField Winner;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					TicTacToe window = new TicTacToe();
					window.frame.setVisible(true);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			
		});
	}

	/**
	 * Create the application.
	 */
	public TicTacToe() 
	{
		initialize();
	}
	public static void turn()
	{
		if(GG == false)
		{
			if (turns%2==1)
			{
				player = "O";
			}
			else
			{
				player = "X";
			}
		}
	}
	public static boolean gl (String a, String b, String c)
	{
		if (a == player && b == player && c == player)
		{
			GG = true;
			WINNER = player;
		}
		else
		{
			GG = false;
		}
		return GG;
	}
	public static void winCheck()
	{ 
		//HORIZONTAL
			GG = gl(A,B,C);
			if (GG == true) 
			{
				return;
			}
			GG = gl(D,E,F);
			if (GG == true) 
			{
				return;
			}
			GG = gl(G,H,I);
			if (GG == true) 
			{
				return;
			}
		//VERTICAL
			GG = gl(A,D,G);
			if (GG == true) 
			{
				return;
			}
			GG = gl(B,E,H);
			if (GG == true) 
			{
				return;
			}
			GG = gl(C,F,I);
			if (GG == true) 
			{
				return;
			}
		//DIAGONAL	
			GG = gl(A,E,I);
			if (GG == true) 
			{
				return;
			}
			GG = gl(C,E,G);
			if (GG == true) 
			{
				return;
			}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 243, 172);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton R1C1 = new JButton("-");
		R1C1.setBounds(4, 6, 75, 29);
		R1C1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				if (GG == false){
				turn();

					if (R1C1.getText().equals("-"))
					{
					R1C1.setText(player);
					turns++;
					A = R1C1.getText();
					winCheck();
					}
				}

			}
		});
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(R1C1);
		
		JButton R1C2 = new JButton("-");
		R1C2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				turn();
				if(GG==false){

					if (R1C2.getText().equals("-"))
					{
					R1C2.setText(player);
					turns++;
					B = R1C2.getText();
					winCheck();
					}
					if(GG==true)
					{
						Winner.setText("Winner is " + player);
					}
				}
			}
		});
		R1C2.setBounds(83, 6, 79, 29);
		frame.getContentPane().add(R1C2);
		
		JButton R1C3 = new JButton("-");
		R1C3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				turn();
				if(GG==false){

					if (R1C3.getText().equals("-"))
					{
					R1C3.setText(player);
					turns++;
					C = R1C3.getText();
					winCheck();
					}
					if(GG==true)
					{
						Winner.setText("Winner is " + player);
					}
				}

			}
		});
		R1C3.setBounds(166, 6, 75, 29);
		frame.getContentPane().add(R1C3);
		
		JButton R2C1 = new JButton("-");
		R2C1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				turn();
				if(GG==false){

					if (R2C1.getText().equals("-"))
					{
					R2C1.setText(player);
					turns++;
					D = R2C1.getText();
					winCheck();
					}
					if(GG==true)
					{
						Winner.setText("Winner is " + player);
					}
				}
			}
		});
		R2C1.setBounds(4, 37, 75, 29);
		frame.getContentPane().add(R2C1);
		
		JButton R2C2 = new JButton("-");
		R2C2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				turn();
				if(GG==false){

					if (R2C2.getText().equals("-"))
					{
					R2C2.setText(player);
					turns++;
					E = R2C2.getText();
					winCheck();
					}
					if(GG==true)
					{
						Winner.setText("Winner is " + player);
					}
				}

			}
		});
		R2C2.setBounds(83, 37, 79, 29);
		frame.getContentPane().add(R2C2);
		
		JButton R2C3 = new JButton("-");
		R2C3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				turn();
				if(GG==false){

					if (R2C3.getText().equals("-"))
					{
					R2C3.setText(player);
					turns++;
					F = R2C3.getText();
					winCheck();
					}
					if(GG==true)
					{
						Winner.setText("Winner is " + player);
					}
				}
			}
		});
		R2C3.setBounds(166, 37, 75, 29);
		frame.getContentPane().add(R2C3);
		
		JButton R3C1 = new JButton("-");
		R3C1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				turn();
				if(GG==false){
					if (R3C1.getText().equals("-"))
					{
					R3C1.setText(player);
					turns++;
					G = R3C1.getText();
					winCheck();
					}
					if(GG==true)
					{
						Winner.setText("Winner is " + player);
					}
				}

			}
		});
		R3C1.setBounds(4, 67, 75, 29);
		frame.getContentPane().add(R3C1);
		
		JButton R3C2 = new JButton("-");
		R3C2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				turn();
				if (GG == false){
					if (R3C2.getText().equals("-"))
					{
					R3C2.setText(player);
					turns++;
					H = R3C2.getText();
					winCheck();
					}
					if(GG==true)
					{
						Winner.setText("Winner is " + player);
					}
			}
}
		});
		R3C2.setBounds(83, 67, 79, 29);
		frame.getContentPane().add(R3C2);
		
		JButton R3C3 = new JButton("-");
		R3C3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				turn();
				if(GG==false){
					if (R3C3.getText().equals("-"))
					{
					R3C3.setText(player);
					turns++;
					I = R3C3.getText();
					winCheck();
					}
					if(GG==true)
					{
						Winner.setText("Winner is " + player);
					}
				}
			}
		});
		R3C3.setBounds(166, 67, 75, 29);
		frame.getContentPane().add(R3C3);
		
		Winner = new JTextField();
		Winner.setBounds(6, 95, 235, 26);
		frame.getContentPane().add(Winner);
		Winner.setColumns(10);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmRestart = new JMenuItem("Restart");
		mntmRestart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GG = false;
				turns = 0;
				WINNER ="";
				Winner.setText(" ");
				R1C1.setText("-");
				R1C2.setText("-");
				R1C3.setText("-");
				R2C1.setText("-");
				R2C2.setText("-");
				R2C3.setText("-");
				R3C1.setText("-");
				R3C2.setText("-");
				R3C3.setText("-");
				A = " ";
				B = " ";
				C = " ";
				D = " ";
				E = " ";
				F = " ";
				G = " ";
				H = " ";
				I = " ";
			}
		});
		mnNewMenu.add(mntmRestart);
		
		JMenuItem mntmExitTictactoe = new JMenuItem("Quit TicTacToe");
		mntmExitTictactoe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnNewMenu.add(mntmExitTictactoe);
	}
}


